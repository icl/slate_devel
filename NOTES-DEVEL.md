Notes for running on summit

The flag SLATE_CUDA_AWARE_MPI may need to be enabled.
The flag is only detected and used in /include/slate/BaseMatrix.hh
This will get CUDA aware MPI, which works well for Cholesky.
This works only on a process-per-gpu run (no multi-gpu socket based runs).
But needs to be investigated and fixed on other algorithms.
CXXFLAGS+=-DSLATE_CUDA_AWARE_MPI

When running use --smpiargs='-gpu' to get CUDA aware MPI
bsub 16 nodes
jsrun -n96 -a1 -c7 -g1 -brs --smpiargs='-gpu' the/tester args

This make.inc file has worked on summit.
CXX=mpicxx
FC=mpif90
blas=essl
LIBS+=-L${CUDA_DIR}/lib64
cuda_arch=volta
CXXFLAGS+=-DSLATE_CUDA_AWARE_MPI
