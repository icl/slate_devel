//------------------------------------------------------------------------------
// Copyright (c) 2017, University of Tennessee
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the University of Tennessee nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL UNIVERSITY OF TENNESSEE BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//------------------------------------------------------------------------------
// This research was supported by the Exascale Computing Project (17-SC-20-SC),
// a collaborative effort of two U.S. Department of Energy organizations (Office
// of Science and the National Nuclear Security Administration) responsible for
// the planning and preparation of a capable exascale ecosystem, including
// software, applications, hardware, advanced system engineering and early
// testbed platforms, in support of the nation's exascale computing imperative.
//------------------------------------------------------------------------------
// For assistance with SLATE, email <slate-user@icl.utk.edu>.
// You can also join the "SLATE User" Google group by going to
// https://groups.google.com/a/icl.utk.edu/forum/#!forum/slate-user,
// signing in with your Google credentials, and then clicking "Join group".
//------------------------------------------------------------------------------

#include "slate/Matrix.hh"
#include "slate/HermitianMatrix.hh"
#include "slate/internal/util.hh"
#include "internal/internal_util.hh"

#include <set>

namespace slate {
namespace internal {

//------------------------------------------------------------------------------
/// [internal]
/// An auxiliary routine to release the panel tiles that are broadcasted. Since
/// the broadcasted tiles are flagged to be hold on the devices memories to be
/// accessed by multiple internal kernels while preventing the tileRelease call
/// in these routine to release them before the others finish accessing
/// them. Note: this function update the tiles origin to make sure that
/// the origin memory is up-to-date and the coherency is kept consistent
/// across multiple address spaces.
/// @tparam matrix_type                                                          
///     Any SLATE matrix type: Matrix and HermitianMatrix.
///
/// @param[in] A
///     The matrix $A$.
///
/// @param[in] k
///     Current column k of the input matrix $A$.
///
/// @ingroup posv_computational
///
template <typename matrix_type>
void panelRelease(matrix_type A, int64_t k)
{
    const int64_t A_nt = A.nt();
    for (int64_t i = k+1; i < A_nt; ++i) {
        if (A.tileIsLocal(i, k)) {
            A.tileUpdateOrigin(i, k);

            std::set<int> dev_set;
            A.sub(i, i, k+1, i).getLocalDevices(&dev_set);
            A.sub(i, A_nt-1, i, i).getLocalDevices(&dev_set);

            for (auto device : dev_set) {
                A.tileUnsetHold(i, k, device);
                A.tileRelease(i, k, device);
            }
        }
    }
}

//------------------------------
// Explicit instantiation.
// matrix_type is slate::HermitianMatrix for slate::potrf
template
void panelRelease<HermitianMatrix<float>>(
    HermitianMatrix<float> A, int64_t k);
template
void panelRelease(HermitianMatrix<double> A, int64_t k);
template
void panelRelease(HermitianMatrix<std::complex<float>> A, int64_t k);
template
void panelRelease(HermitianMatrix<std::complex<double>> A, int64_t k);
// matrix_type is slate::Matrix for slate::getrf
template
void panelRelease(Matrix<float> A, int64_t k);
template
void panelRelease(Matrix<double> A, int64_t k);
template
void panelRelease(Matrix<std::complex<float>> A, int64_t k);
template
void panelRelease(Matrix<std::complex<double>> A, int64_t k);

//------------------------------------------------------------------------------
/// [internal]
/// Computes the power function for integer arguments.
///
template <typename T>
T pow(T base, T exp)
{
    T pow = 1;
    for (T i = 0; i < exp; ++i)
        pow *= base;

    return pow;
}

//------------------------------
// explicit instantiation
template
int pow<int>(int base, int exp);

//------------------------------------------------------------------------------
/// [internal]
/// Implememts a custom MPI reduction that propagates NaNs.
///
void mpi_max_nan(void* invec, void* inoutvec, int* len, MPI_Datatype* datatype)
{
    if (*datatype == MPI_DOUBLE) {
        double* x = (double*) invec;
        double* y = (double*) inoutvec;
        for (int i = 0; i < *len; ++i)
            y[i] = max_nan(x[i], y[i]);
    }
    else if (*datatype == MPI_FLOAT) {
        float* x = (float*) invec;
        float* y = (float*) inoutvec;
        for (int i = 0; i < *len; ++i)
            y[i] = max_nan(x[i], y[i]);
    }
}

} // namespace internal
} // namespace slate
